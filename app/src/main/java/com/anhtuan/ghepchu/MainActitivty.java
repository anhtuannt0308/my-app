package com.anhtuan.ghepchu;

import android.content.ClipData;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ${ADMIN} on 03/11/17.
 */

public class MainActitivty extends AppCompatActivity implements View.OnDragListener, View.OnClickListener {
	private TextView tvStatus;
	private Button buttonClear,buttonReset,buttonResetGame;
	private int indexResult = 0;
	private float x;
	private int size = 0;
	int  falseNumber=0;
	private String result = "";
	private String resultSelect = "";
	private ArrayList<String> listResult;
	private ArrayList<Button> listButtonResult;
	private ArrayList<Button> listButtonSelect;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playgame);
		initResult();
		initView();
		playGame();
	}
	private void initResult() {
		listResult = new ArrayList<>();
		listResult.add("write");
		listResult.add("welcome");
		listResult.add("smile");
		listResult.add("cry");
		listResult.add("remove");
		listResult.add("give");
		listResult.add("success");
		listResult.add("access");
		Collections.shuffle(listResult);
	}
	private void resetGame(String result) {
		for (int i = 0; i < listButtonResult.size(); i++) {
			listButtonResult.get(i).setVisibility(View.GONE);
			listButtonSelect.get(i).setVisibility(View.GONE);
		}
		for (int i=0;i<result.length();i++){
			listButtonResult.get(i).setVisibility(View.INVISIBLE);
			listButtonSelect.get(i).setVisibility(View.INVISIBLE);
		}
	}
	private void initView() {
		buttonResetGame=(Button)findViewById(R.id.btn_resetgame);
		buttonResetGame.setOnClickListener(this);
		buttonResetGame.setVisibility(View.GONE);
		buttonClear = (Button)findViewById(R.id.btn_clear);
		buttonClear.setOnClickListener(this);
		buttonReset = (Button)findViewById(R.id.btn_reset);
		buttonReset.setOnClickListener(this);
		tvStatus = (TextView)findViewById(R.id.tv_Status);
		TableRow tbrResult1 = (TableRow)findViewById(R.id.tbr_1);
		TableRow tbrResult2 = (TableRow)findViewById(R.id.tbr_2);
		TableRow tbrSelect4 = (TableRow)findViewById(R.id.tbr_4);
		TableRow tbrSelect5 = (TableRow)findViewById(R.id.tbr_5);
		tbrResult1.setWeightSum(6f);
		tbrResult2.setWeightSum(6f);
		tbrSelect5.setWeightSum(6f);
		tbrSelect4.setWeightSum(6f);
		Button btnResult1 = (Button)findViewById(R.id.btnResult1);
		Button btnResult2 = (Button)findViewById(R.id.btnResult2);
		Button btnResult3 = (Button)findViewById(R.id.btnResult3);
		Button btnResult4 = (Button)findViewById(R.id.btnResult4);
		Button btnResult5 = (Button)findViewById(R.id.btnResult5);
		Button btnResult6 = (Button)findViewById(R.id.btnResult6);
		Button btnResult7 = (Button)findViewById(R.id.btnResult7);
		Button btnResult8 = (Button)findViewById(R.id.btnResult8);
		Button btnResult9 = (Button)findViewById(R.id.btnResult9);
		Button btnResult10 = (Button)findViewById(R.id.btnResult10);
		Button btnResult11 = (Button)findViewById(R.id.btnResult11);
		Button btnResult12 = (Button)findViewById(R.id.btnResult12);
		listButtonResult=new ArrayList<>();
		listButtonResult.add(btnResult1);
		listButtonResult.add(btnResult2);
		listButtonResult.add(btnResult3);
		listButtonResult.add(btnResult4);
		listButtonResult.add(btnResult5);
		listButtonResult.add(btnResult6);
		listButtonResult.add(btnResult7);
		listButtonResult.add(btnResult8);
		listButtonResult.add(btnResult9);
		listButtonResult.add(btnResult10);
		listButtonResult.add(btnResult11);
		listButtonResult.add(btnResult12);
		Button btnSelect1 = (Button)findViewById(R.id.btnSelect1);
		Button btnSelect2 = (Button)findViewById(R.id.btnSelect2);
		Button btnSelect3 = (Button)findViewById(R.id.btnSelect3);
		Button btnSelect4 = (Button)findViewById(R.id.btnSelect4);
		Button btnSelect5 = (Button)findViewById(R.id.btnSelect5);
		Button btnSelect6 = (Button)findViewById(R.id.btnSelect6);
		Button btnSelect7 = (Button)findViewById(R.id.btnSelect7);
		Button btnSelect8 = (Button)findViewById(R.id.btnSelect8);
		Button btnSelect9 = (Button)findViewById(R.id.btnSelect9);
		Button btnSelect10 = (Button)findViewById(R.id.btnSelect10);
		Button btnSelect11 = (Button)findViewById(R.id.btnSelect11);
		Button btnSelect12 = (Button)findViewById(R.id.btnSelect12);
		listButtonSelect=new ArrayList<>();
		listButtonSelect.add(btnSelect1);
		listButtonSelect.add(btnSelect2);
		listButtonSelect.add(btnSelect3);
		listButtonSelect.add(btnSelect4);
		listButtonSelect.add(btnSelect5);
		listButtonSelect.add(btnSelect6);
		listButtonSelect.add(btnSelect7);
		listButtonSelect.add(btnSelect8);
		listButtonSelect.add(btnSelect9);
		listButtonSelect.add(btnSelect10);
		listButtonSelect.add(btnSelect11);
		listButtonSelect.add(btnSelect12);
	}
	View.OnTouchListener touchListener= new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			switch (motionEvent.getAction()){
				case MotionEvent.ACTION_DOWN:
					ClipData data = ClipData.newPlainText("appid", "");
					View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(
							view);
					view.startDrag(data, shadowBuilder, view, 0);
					break;
				case MotionEvent.ACTION_MOVE:
					break;
				case MotionEvent.ACTION_UP:
					break;
			}
			return false;
		}
	};
	private void onClickButtonClear() {
		size = 0;
		resultSelect = "";
		playGame();
	}
	private void onClickButtonReset() {
		size = 0;
		resultSelect = "";
		Collections.shuffle(listResult);
		playGame();
	}
	private void playGame() {
		if (indexResult < listResult.size()) {
			result = listResult.get(indexResult);
			resetGame(result);
			ArrayList<Character> listCharResult = new ArrayList<>();
			for (int i = 0; i < result.length(); i++) {
				listCharResult.add(result.charAt(i));
			}
			Collections.shuffle(listCharResult);
			buttonClear.setVisibility(View.VISIBLE);
			buttonReset.setVisibility(View.VISIBLE);
			for (int i = 0; i < listCharResult.size(); i++) {
				listButtonResult.get(i).setVisibility(View.VISIBLE);
				listButtonSelect.get(i).setVisibility(View.VISIBLE);
				listButtonSelect.get(i).setOnTouchListener(touchListener);
				listButtonResult.get(i).setOnDragListener(this);
				listButtonResult.get(i).setText("");
				listButtonSelect.get(i).setEnabled(true);
				listButtonResult.get(i).setBackgroundColor(Color.parseColor("#E0E0E0"));
				listButtonSelect.get(i).setText(listCharResult.get(i).toString());
			}
		}
		if (indexResult == listResult.size()) {
			buttonClear.setVisibility(View.GONE);
			buttonReset.setVisibility(View.GONE);
			buttonResetGame.setVisibility(View.VISIBLE);
			buttonResetGame.setEnabled(true);
			for (int i = 0; i < result.length(); i++) {
				listButtonSelect.get(i).setVisibility(View.GONE);
				listButtonResult.get(i).setVisibility(View.GONE);
			}
		}
	}
	@Override
	public boolean onDrag(View view, DragEvent dragEvent) {
		Drawable enterShape = getResources().getDrawable(
				R.drawable.shape_droptarget);
		Drawable normalShape = getResources().getDrawable(R.drawable.shape);
		final View v = (View) dragEvent.getLocalState();
		switch (dragEvent.getAction()) {
			case DragEvent.ACTION_DRAG_STARTED:
				v.setBackground(enterShape);
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				v.setBackgroundColor(Color.parseColor("#E0E0E0"));
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				if (((Button) view).getText().length() == 0) {
					view.setBackgroundColor(Color.parseColor("#E0E0E0"));
				}
				break;
			case DragEvent.ACTION_DROP:
				final View v1 = (View) dragEvent.getLocalState();
				String str = "";
				int index = 0;
				for (int i = 0; i < result.length(); i++) {
					if (v1.getId() == listButtonSelect.get(i).getId()) {
						index = i;
						str = listButtonSelect.get(i).getText().toString();
						x = view.getX();
					}
				}
				for (int i = 0; i < result.length(); i++) {
					final int id = v1.getId();
					if (listButtonResult.get(i).getX() == x) {
						if (listButtonResult.get(i).getText().length() == 0 && str.length() != 0) {
							listButtonResult.get(i).setText(str);
							listButtonResult.get(i).setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View view) {
									if (((Button) view).getText().length() > 0) {
										String str = ((Button) view).getText().toString();
										((Button) view).setText("");
										((Button) view).setBackgroundColor(Color.parseColor("#E0E0E0"));
										size=size-1;
										resultSelect="";
										tvStatus.setText("");
										for (int i = 0; i < result.length(); i++) {
											if (listButtonSelect.get(i).getId() == id) {
												listButtonSelect.get(i).setText(str);
												listButtonSelect.get(i).setEnabled(true);
											}
										}
									}
								}
							});
							size = size + 1;
							x = 0;
							listButtonSelect.get(index).setText("");
							listButtonSelect.get(index).setEnabled(false);
							view.setBackgroundDrawable(normalShape);
						}
					}
				}
				break;
			default:
				break;
		}
		if (size == result.length()) {
			for (int j = 0; j < result.length(); j++) {
				resultSelect = resultSelect + listButtonResult.get(j).getText().toString();
				resultSelect=resultSelect.toLowerCase();
			}
		}
		if (result.length() != size) {
			tvStatus.setText("");
		}
		if (resultSelect.equals(result) && size == result.length()) {
			indexResult++;
			size = 0;
			resultSelect = "";
			playGame();
		} else {
			if (!resultSelect.equals(result) && size == result.length()) {
				tvStatus.setText("False");
				falseNumber=falseNumber+1;
			}
			if (indexResult == listResult.size()) {
				buttonClear.setVisibility(View.GONE);
				buttonReset.setVisibility(View.GONE);
				buttonResetGame.setVisibility(View.VISIBLE);
				buttonResetGame.setEnabled(true);
				for (int i=0;i<result.length();i++){
					listButtonSelect.get(i).setVisibility(View.GONE);
					listButtonResult.get(i).setVisibility(View.GONE);
				}
				tvStatus.setText("Số lần sai: " + falseNumber);
			}
		}
		return true;
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.btn_clear:
				tvStatus.setText("");
				onClickButtonClear();
				break;
			case R.id.btn_reset:
				tvStatus.setText("");
				onClickButtonReset();
				break;
			case R.id.btn_resetgame:
				size=0;
				resultSelect="";
				indexResult=0;
				tvStatus.setText("");
				falseNumber=0;
				buttonResetGame.setVisibility(View.GONE);
				playGame();
				break;
		}
	}
}
